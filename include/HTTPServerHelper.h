#include <WiFiClient.h>
#include <ESP8266Webserver.h>
#include <FastLED.h>
#include <TimeLib.h>

struct HSVColor {
  int h = random(0, 350);
  int s = random(125, 255);
  int v = 255;
};

// Default settings struct
struct Settings {
  int brightness = 20;
  int color_rotation_time = 20;
  int effect = 1;
  bool color_rotation = true;
};

class HTTPServerHelper
{
private:
  CFastLED *leds;
  HSVColor *colors;
  ESP8266WebServer *server;
public:
  Settings settings;

  // WIFI connection credentials struct
  struct {
    char SSID[20]  = "";
    char Pass[20]  = "";
    int mode = 0;
  } wifi;

  HTTPServerHelper(ESP8266WebServer *http, CFastLED *fastLed, HSVColor *hsv){
    leds = fastLed;
    colors = hsv;
    server = http;
  };

  void initAccessPoint(){
    // Setup default network settings
    IPAddress ip(192,168,1,1);
    IPAddress subnet(255,255,255,0);
    WiFi.softAPConfig(ip, false, subnet);

    // Setup wifi ssid settings
    WiFi.mode(WIFI_AP);           //Only Access point
    WiFi.softAP("easygraphsio_clock");
    Serial.println("HTTP server started");

    wifi.mode = 0;

  }

  void begin(){
    server->begin();
  }

  void handle(){
      server->handleClient();
  }

  // Return settings in the json format for UI
  String getSettings(){
      String json = "{ \"brightness\": \"" + String(settings.brightness) + "\", \"color_rotation\": \"" + String(settings.color_rotation) + "\", \"color_rotation_time\": \"" + String(settings.color_rotation_time) + "\"}";
      return json;
  }

  // Scan and return wifi ssids list in the json format for UI
  String SSIDSlist() {
    int numberOfNetworks = WiFi.scanNetworks();
    String json;
    for(int i =0; i<numberOfNetworks; i++){
        json += "\"" + String(WiFi.SSID(i)) + "\",";
    }
    json = "[" + json.substring(0, json.length()-1) + "]";
    return json;
  }

  void settingsHandler(){
    server->send(200, "application/json", "{ \"ssids\":"+SSIDSlist()+", \"settings\":"+getSettings()+"}");
  }

  void wifiHandler(){
    Serial.println(server->args());
    if(server->args()) {
      for (int i = 0; i < server->args(); i++){
          Serial.println(" " + server->argName(i) + ": " + server->arg(i));
          if(String(server->argName(i)) == "wifi_ssid" && server->arg(i).length()) {
            strncpy(wifi.SSID, server->arg(i).c_str(), 20);
          }
          if(String(server->argName(i)) == "wifi_pass" && server->arg(i).length()) {
            strncpy(wifi.Pass, server->arg(i).c_str(), 20);
          }
      }
      initWifiConnection();
    }
    server->send(200, "text/html", "CLOCK IP IS:"+WiFi.localIP());
  }

  void initWifiConnection(){
    WiFi.disconnect(true);
    WiFi.mode(WIFI_OFF);
    delay(50);

    WiFi.begin(wifi.SSID, wifi.Pass);
    while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
      delay(1000);
      Serial.println('.');
    }
    Serial.println('\n');
    Serial.println("Connection established!");
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());

    wifi.mode = 1;
  }

  // HTTP Handler for settings update
  void updateHandler() {
    if(server->args()) {
      for (int i = 0; i < server->args(); i++){
          Serial.println(" " + server->argName(i) + ": " + server->arg(i));
          if(String(server->argName(i)) == "brightness" && server->arg(i).length()) {
            settings.brightness = server->arg(i).toInt();
          }
          if(String(server->argName(i)) == "rotation" && server->arg(i).length()) {
            settings.color_rotation = server->arg(i).toInt();
          }
          if(String(server->argName(i)) == "rotation_time" && server->arg(i).length()) {
            settings.color_rotation_time = server->arg(i).toInt();
          }

          if(String(server->argName(i)) == "color_h" && server->arg(i).length()) {
            if(server->arg(i).toInt()) {
              colors->h = map(server->arg(i).toInt(), 0, 360, 0, 255);
            }
          }
          if(String(server->argName(i)) == "color_s" && server->arg(i).length()) {
            if(server->arg(i).toInt()) {
              colors->s = map(server->arg(i).toInt(), 0, 100, 0, 255);
            }
          }

          if(String(server->argName(i)) == "color_v" && server->arg(i).length()) {
            if(server->arg(i).toInt()) {
              colors->v = map(server->arg(i).toInt(), 0, 100, 0, 255);
            }
          }

          if(String(server->argName(i)) == "time_h" && server->arg(i).length()) {
            if(server->arg(i).toInt()) {
               setTime(server->arg(i).toInt(), minute(), false, false, false, false);
            }
          }

          if(String(server->argName(i)) == "time_m" && server->arg(i).length()) {
            if(server->arg(i).toInt()) {
               setTime(hour(), server->arg(i).toInt(), false, false, false, false);
            }
          }
      }
      leds->setBrightness( settings.brightness );
    }
    server->send(200, "text/html", "<script type=text/javascript>document.location.href='/';</script>");
  }

  String getContentType(String filename) { // convert the file extension to the MIME type
    if (filename.endsWith(".html")) return "text/html";
    else if (filename.endsWith(".css")) return "text/css";
    else if (filename.endsWith(".js")) return "application/javascript";
    else if (filename.endsWith(".ico")) return "image/x-icon";
    return "text/plain";
  }

  bool handleRequestRead(String path) { // send the right file to the client (if it exists)
    Serial.println("handleFileRead: " + path);

    if (path.endsWith("/update")) {
      updateHandler(); return true;
    }
    if (path.endsWith("/wifi")) {
      wifiHandler(); return true;
    }
    if (path.endsWith("/settings")) {
      settingsHandler(); return true;
    }

    if (path.endsWith("/")) path += "index.html";         // If a folder is requested, send the index file
    if (path.endsWith("/connect")) path = "/wifiform.html";

    String contentType = getContentType(path);            // Get the MIME type
    if (SPIFFS.exists(path)) {                            // If the file exists
      File file = SPIFFS.open(path, "r");                 // Open it
      size_t sent = server->streamFile(file, contentType); // And send it to the client
      file.close();                                       // Then close the file again
      return true;
    }
    Serial.println("\tFile Not Found");
    return false;                                         // If the file doesn't exist, return false
  }

};
