#include <Arduino.h>

#include <TZ.h>

#include <coredecls.h>                  // settimeofday_cb()
#include <Schedule.h>
#include <PolledTimeout.h>

#include <time.h>                       // time() ctime()
#include <sys/time.h>                   // struct timeval

#include <sntp.h>                       // sntp_servermode_dhcp()

#define RTC_UTC_TEST 1510592825 // 1510592825 = Monday 13 November 2017 17:07:05 UTC
#define MYTZ TZ_Europe_Tallinn


// for testing purpose:
extern "C" int clock_gettime(clockid_t unused, struct timespec *tp);

// TimeHelper helper class
class TimeHelper
{
  private:
    timeval tv;
    timespec tp;
    time_t nows;
    tm *t;
  public:
    TimeHelper() {
        configTime(MYTZ, "pool.ntp.org");
    };
    void refresh() {
      gettimeofday(&tv, nullptr);
      clock_gettime(0, &tp);
      nows = time(nullptr);
      t = localtime(&nows);
    }
    int getHours() {
      return t->tm_hour;
    }
    int getMinutes() {
      return t->tm_min;
    }
    int getSeconds() {
      return t->tm_sec;
    }
};
