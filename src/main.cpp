#include <Arduino.h>

#include "Thread.h"
#include "ThreadController.h"
#include <TimeLib.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <FastLED.h>
#include <TimeHelper.h>
#include <OTAHelper.h>
#include <HTTPServerHelper.h>
#include <FS.h>

TimeHelper timeClient;
OTAHelper ota;

HSVColor color;

// Init http server on port 80
ESP8266WebServer server(80);
HTTPServerHelper httpServer(&server, &FastLED, &color);

// WS2812B (RGB strip) control pin
#define LED_PIN     5

// WS2812B (RGB strip) connected leds count
#define LED_COUNT  88

// Define the RGB strip type
#define LED_TYPE    WS2811

// Define the RGB strip color control order
#define COLOR_ORDER GRB

CRGB leds[LED_COUNT];

// Int array for the seconds leds control
const unsigned int seconds[4] = {42,43,44,45};

// Digits pin mask
const unsigned char valueMask[10][21] PROGMEM = {
  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0},
  {0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},
  {1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,0,0,0,1,1,1},
  {1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1},
  {0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1},
  {1,1,1,0,0,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1},
  {1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
  {1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},
  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
  {1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1},
};

// Int map for leds states (on, off)
int ledsState[LED_COUNT][2];

// Array for each segment
int segmentData[4] = {10,10,10,10};

// Draw the passed number to passed segment
void drawSegment(int number, int segment){

  int end = (segment * 21);
  int start = end - 21;

  if (segment > 2) {
    start += 4;
    end   += 4;
  }

  int defDelay = 30;

  for (int i = start; i < end; i++) {
    int numb = map(i, start, end, 0, 21);

    if(pgm_read_byte(&(valueMask[number][numb])) == 1) {
      ledsState[i][0] = 1;
      ledsState[i][1] = color.h;
      leds[i].setHSV(color.h, color.s, color.v);
    } else {
      ledsState[i][0] = 0;
      ledsState[i][1] = 0;
      leds[i]= CRGB::Black;
    }

    FastLED.show();
    delay(defDelay--);
  }
}

// Format digits value to string
String formatDigits(int digits){
  String data = "";
  if(digits < 10) {
    return data += "0"+String(digits);
  }
  return data += String(digits);
}

// SecondsThread interrupt thread
class SecondsTimeThread: public Thread
{
public:
  float value;
	void run(){

    int color_h = color.h;
    int color_s = color.s;
    int color_v = color.v;

    if(!value) {
        color_h = 0;
        color_s = 0;
        color_v = 0;
    }

    for (int i = 0; i < 4; i++) {
        leds[seconds[i]].setHSV(color_h, color_s, color_v);
        FastLED.show();
        delay(15);
    }

    value = !value;

    if (httpServer.wifi.mode == 1) {
      timeClient.refresh();
      setTime(timeClient.getHours(), timeClient.getMinutes(), timeClient.getSeconds(), false, false, false);
    }
		runned();
	}
};

// Instantiate a new thread for SecondsTimeThread
SecondsTimeThread SecondsThread = SecondsTimeThread();

// BaseTimeThread interrupt thread
class BaseTimeThread: public Thread
{
public:
	void run(){
		runned();
	}
  void UpdateTimeSegment(String time){

    Serial.println("time update trigger");

    String ch = "";
    for(int i = 0; i < 4; i++){
      ch = (char)time[i];
      if(segmentData[i] != ch.toInt()) {
        segmentData[i] = ch.toInt();
        drawSegment(segmentData[i], i+1);
      }
    }
  }
};

// Instantiate a new thread for BaseTimeThread
BaseTimeThread TimeThread = BaseTimeThread();

// ColorChangeThread interrupt thread
class ColorChangeThread: public Thread
{
public:
	void run(){

    randomSeed(ESP.getCycleCount());
    color.h = random(0, 350);
    color.s = random(125, 255);
    color.v = 255;

		runned();
	}
};

// Instantiate a new thread for ColorChangeThread
ColorChangeThread ColorThread = ColorChangeThread();

// ColorChangeThread interrupt thread
class RotateColorThread: public Thread
{
public:
	void run(){
      for (int i = 0; i < LED_COUNT; i++){
        int rand = random(0, LED_COUNT);
        if (ledsState[rand][0] == 1 && ledsState[rand][1] != color.h){
          leds[rand].setHSV(color.h, color.s, color.v);
          ledsState[rand][1] = color.h;

          FastLED.show();
          return runned();
        }
      }
	}
};
// Instantiate a new thread for ColorChangeThread
RotateColorThread RotateColor = RotateColorThread();

// Instantiate a new ThreadController - Base controller for all threads
ThreadController controller = ThreadController();

bool handleRead(String path) {
  return httpServer.handleRequestRead(path);
}

void setup() {
  Serial.begin(9600);
  SPIFFS.begin();

  httpServer.initAccessPoint();
  server.onNotFound([]() {
  if (!handleRead(server.uri()))
      server.send(404, "text/plain", "404: Not Found");
  });

  // Start http server
  httpServer.begin();

  // Setup FastLed
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, LED_COUNT).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness( httpServer.settings.brightness );

  // Setup threads
  ColorThread.setInterval(20000);
  ColorThread.enabled = true;
  controller.add(&ColorThread);

  SecondsThread.setInterval(850);
  SecondsThread.enabled = true;
  controller.add(&SecondsThread);

  RotateColor.setInterval(30);
  RotateColor.enabled = true;
  controller.add(&RotateColor);

  TimeThread.setInterval(2000);
  TimeThread.enabled = true;
  controller.add(&TimeThread);

  ota.Init();

  /** Init wifi connection without configuration from WEB
    String ssid = "wifi_name";
    String pass = "wifi_pass";

    ssid.toCharArray(httpServer.wifi.SSID, 20);
    pass.toCharArray(httpServer.wifi.Pass, 20);

    httpServer.initWifiConnection();
  **/

  // Setup default date for clock
  setTime(00, 00, 00, false, false, false);
}

String prev_time;

void loop() {

  ColorThread.setInterval(httpServer.settings.color_rotation_time * 1000);
  ColorThread.enabled = httpServer.settings.color_rotation;

  ota.Handle();

  String curr_time;

  // Parse current time
  curr_time += formatDigits(hour());
  curr_time += formatDigits(minute());

  if (prev_time == "") {
    prev_time = curr_time;
    TimeThread.UpdateTimeSegment(curr_time);
  } else if (prev_time != curr_time) {
    prev_time = curr_time;
    TimeThread.UpdateTimeSegment(curr_time);
  }

  httpServer.handle();
  controller.run();
}
